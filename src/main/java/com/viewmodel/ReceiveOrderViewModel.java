package com.viewmodel;

import com.model.Data;
import com.model.ReceiveOrder;
import org.zkoss.bind.annotation.Init;

import java.util.ArrayList;
import java.util.List;

public class ReceiveOrderViewModel {
    private List<ReceiveOrder> receiveOrders=new ArrayList<ReceiveOrder>();
    private Data data=new Data();
    @Init
    public List<ReceiveOrder> getReceiveOrders(){
        for(int i=0;i<data.getItems().size();i++){
            receiveOrders.add(new ReceiveOrder(i,"ORDER"+i, data.getStorers().get(i),data.getItems().get(i),data.getPacks().get(i),data.getLocations().get(i)));
        }
        return receiveOrders;
    };
}
