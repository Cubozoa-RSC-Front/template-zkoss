package com.viewmodel;

import com.model.Data;
import com.model.Storer;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zul.*;

import java.util.ArrayList;
import java.util.List;

public class StorerViewModel {

    private Data storers=new Data();
    @Init
    public List<Storer> getStorers() {
        return storers.getStorers();
    }

}
