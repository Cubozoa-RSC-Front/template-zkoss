package com.viewmodel;

public class LocationFilter {
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name==null?"":name.trim();
    }

    private String name;

}
