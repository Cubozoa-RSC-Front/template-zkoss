package com.viewmodel;

import com.model.Data;
import com.model.Item;
import com.model.Storer;
import org.zkoss.bind.annotation.Init;

import java.util.ArrayList;
import java.util.List;

public class ItemViewModel {
    private Data items=new Data();
    @Init
    public List<Item> getItems() {
        return items.getItems();
    }
}
