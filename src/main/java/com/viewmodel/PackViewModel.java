package com.viewmodel;

import com.model.Data;
import com.model.Item;
import com.model.Pack;
import org.zkoss.bind.annotation.Init;

import java.util.ArrayList;
import java.util.List;

public class PackViewModel {
    private Data packs=new Data();
    @Init
    public List<Pack> getPacks() {
        return packs.getPacks();
    }
}
