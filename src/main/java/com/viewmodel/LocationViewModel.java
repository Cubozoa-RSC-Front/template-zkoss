package com.viewmodel;
import com.model.LocData;
import com.model.Location;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import java.util.List;

public class LocationViewModel {
    private LocationFilter locFilter = new LocationFilter();
    List<Location> locations = LocData.getAllLocations();
    public LocationFilter getLocFilter() {
        return locFilter;
    }
    public List<Location> getLocations(){
        return locations;
    }
    @Command
    @NotifyChange({"locations"})
    public void changeFilter() {
        locations = LocData.getFilterLocations(locFilter);
    }
}
