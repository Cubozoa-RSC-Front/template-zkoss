package com.viewmodel;

import com.model.LocData;
import com.model.Location;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zul.*;

import java.util.List;

public class OrderViewModel {
    private static final String orderMessage = "A Total of %d Order Items";
    private LocationFilter locationFilter = new LocationFilter();
    List<Location> currentOrder = LocData.getAllLocations();

    public LocationFilter getLocationFilter() {
        return locationFilter;
    }

    public ListModel<Location> getLocationModel() {
        return new ListModelList<Location>(currentOrder);
    }

    public String getOrder() {
        return String.format(orderMessage, currentOrder.size());
    }

    @Command
    @NotifyChange({"locationModel", "location"})
    public void changeFilter() {
        currentOrder = LocData.getFilterLocations(locationFilter);
    }

    @Command
    public void toTest(Location location) {
        Executions.sendRedirect("test2.zul");
    }
}
