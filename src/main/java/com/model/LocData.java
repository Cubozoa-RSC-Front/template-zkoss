package com.model;

import com.viewmodel.LocationFilter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LocData {
    private static List<Location> locations = new ArrayList<Location>();
    static {
        locations.add(new Location(1,"AL011","type1","OK","class1","zone1"));
        locations.add(new Location(2,"AL02","type2","NotOK","class2","zone2"));
        locations.add(new Location(3,"AL03","type1","OK","class2","zone1"));
        locations.add(new Location(4,"AL04","type3","OK","class3","zone3"));
        locations.add(new Location(5,"AL05","type1","NotOK","class1","zone1"));
    }
    public static List<Location> getAllLocations() {
        return new ArrayList<Location>(locations);
    }
    public static Location[] getAllLocationsArray() {
        return locations.toArray(new Location[locations.size()]);
    }

    public static List<Location> getFilterLocations(LocationFilter locFilter) {
        List<Location> someLocations = new ArrayList<Location>();
        String loc = locFilter.getName().toLowerCase();

        for (Iterator<Location> i = locations.iterator(); i.hasNext();) {
            Location tmp = i.next();
            if (tmp.getName().toLowerCase().contains(loc)) {
                someLocations.add(tmp);
            }
        }
        return someLocations;
    }
}
