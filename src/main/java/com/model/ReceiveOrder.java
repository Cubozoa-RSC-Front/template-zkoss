package com.model;

public class ReceiveOrder {
    private int id;
    private String no;
    private Storer storer;
    private Item item;
    private Pack pack;
    private Location location;

    public ReceiveOrder(int id,String no,Storer storer,Item item,Pack pack,Location location){
        this.id=id;
        this.no=no;
        this.storer=storer;
        this.item=item;
        this.pack=pack;
        this.location=location;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Storer getStorer() {
        return storer;
    }

    public void setStorer(Storer storer) {
        this.storer = storer;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Pack getPack() {
        return pack;
    }

    public void setPack(Pack pack) {
        this.pack = pack;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
