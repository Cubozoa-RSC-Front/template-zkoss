package com.model;

import java.util.ArrayList;
import java.util.List;

public class Data {
    private List<Storer> storers=new ArrayList<Storer>();
    private List<Item> items=new ArrayList<Item>();
    private List<Location> locations=new ArrayList<Location>();
    private List<Pack> packs=new ArrayList<Pack>();
    {
        for(int i=1;i<6;i++){
            storers.add(new Storer(i,"货主"+i,"STORER"+i));
            items.add(new Item(i,"商品"+i,"ITEM"+i));
            locations.add(new Location(i,"AL0"+i,"type"+1,"status"+i,"classification"+i,"zone"+i));
            packs.add(new Pack(i,"包装"+i));
        }
    }

    public List<Storer> getStorers() {
        return storers;
    }

    public List<Item> getItems() {
        return items;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public List<Pack> getPacks() {
        return packs;
    }
}
