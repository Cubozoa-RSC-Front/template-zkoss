package com.model;

public class Storer {
    private int id;
    private String name;
    private String no;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public Storer(int id, String name, String no){
        this.id=id;
        this.name=name;
        this.no=no;
    }
}
