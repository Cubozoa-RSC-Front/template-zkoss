package com.model;

public class Location {
    private int id;
    private String name;
    private String type;
    private String status;
    private String classification;
    private String zone;
    public Location(int id,String name,String type,String status,String classification,String zone){
        this.id=id;
        this.name=name;
        this.type=type;
        this.status=status;
        this.classification=classification;
        this.zone=zone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getClassification() {
        return classification;
    }

    public void setClassification(String classification) {
        this.classification = classification;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }
}
