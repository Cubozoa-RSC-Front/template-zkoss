package com.render;

import com.model.Location;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zul.*;

public class LocationRowRender implements RowRenderer<Location> {

    @Override
    public void render(Row row, Location location, int i) throws Exception {
        Span span=new Span();
        span.setSclass("z-icon-play");
        span.addEventListener(Events.ON_CLICK, new EventListener<Event>() {
            @Override
            public void onEvent(Event event) throws Exception {
                Executions.sendRedirect("detail.zul?"
                +"name="+location.getName()+"&"
                +"type="+location.getType()+"&"
                +"classification="+location.getStatus()+"&"
                +"status="+location.getStatus()+"&"
                +"zone="+location.getZone());
            }
        });
        row.appendChild(new Checkbox());
        row.appendChild(span);
        row.appendChild(new Label(location.getName()));
        row.appendChild(new Label(location.getType()));
        row.appendChild(new Label(location.getClassification()));
        row.appendChild(new Label(location.getStatus()));
        row.appendChild(new Label(location.getZone()));
    }
}
